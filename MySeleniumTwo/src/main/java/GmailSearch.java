import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;

public class GmailSearch {

    WebDriver driver;
    private String mail = "autohelp.lviv@gmail.com";
    private String mailTo = "mykhailokozak1998@gmail.com";
    private String password = "13101998gmail";
    private String letter = "Selenium is good.";
    private String theme = "It is important!";

    @BeforeTest
    public  void driverSetting(){
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void gmailEnterTest() {
        driver.get("https://www.gmail.com/");

        WebElement elementMail = driver.findElement(By.name("identifier"));
        elementMail.sendKeys(mail);
        WebElement buttonNext = driver.findElement(By.id("identifierNext"));
        buttonNext.click();

        new WebDriverWait(driver, 25).until(dr -> dr.findElement(By.id("profileIdentifier")).getText().equalsIgnoreCase(mail));


        WebElement elementPassword = driver.findElement(By.name("password"));
        elementPassword.sendKeys(password);
        WebElement buttonPasswordNext = driver.findElement(By.id("passwordNext"));
        buttonPasswordNext.click();

        new WebDriverWait(driver, 25).until(dr -> dr.findElement(By.xpath("//div[@id=':2l']")).isDisplayed());
    }

    @Test(dependsOnMethods = "gmailEnterTest")
    public void sendMailTest(){
        WebElement buttonWrite = driver.findElement(By.xpath("//div[@role='button' and @gh='cm']"));
        buttonWrite.click();

        new WebDriverWait(driver, 25).until(dr -> dr.findElement(By.xpath("//form[@method='POST']")).isDisplayed());

        WebElement elementMailTo = driver.findElement(By.xpath("//textarea[@name='to']"));
        elementMailTo.sendKeys(mailTo);
        WebElement elementMailToTheme = driver.findElement(By.xpath("//input[@name='subjectbox']"));
        elementMailToTheme.sendKeys(theme);
        WebElement elementLetter = driver.findElement(By.xpath("//div[@role='textbox' and @aria-multiline='true']"));
        elementLetter.sendKeys(letter);

       //WebElement buttonSend = driver.findElement(By.xpath("//tr//td//div[@id=':5s']//div[@id=':7y' and @role='button']"));
        //this xpath worked in psvm, but is fieled in Junit!!!
        WebElement buttonSend =driver.findElement(By.xpath("//div[@class='T-I J-J5-Ji aoO T-I-atl L3' and @role='button']"));
        buttonSend.click();
        Assert.assertTrue(new WebDriverWait(driver, 25).until(dr -> dr.findElement(By.xpath("//span[@role='link' and @id='link_vsm']")).isDisplayed()));
    }

        @AfterTest
        public  void driverQuit(){
            driver.quit();
        }
}
